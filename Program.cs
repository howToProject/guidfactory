﻿// See https://aka.ms/new-console-template for more information

namespace GuidFactory;

/// <summary>
/// dotnet run 4 -> prints 4 Guids
/// dotnet run -> prints 1 guid
/// </summary>
public class Programm
{
    public static void Main(string[] args)
    {
        int counter = 1;

        try
        {
            counter = Int32.Parse(args[0]);
        }
        catch (Exception) { /* ignored as no arg is a valid usecase */ }

        if (counter > 20)
        {
            Console.WriteLine("Requested value is too large. It was set to 20");
            counter = 20;
        }

        if (counter == 1)
        {
            Console.WriteLine("Here comes a Guid: ");
        }

        if (counter < 1)
        {
            Console.WriteLine("Less than one Guid can't be printed. Here is one");
            counter = 1;
        }

        // 1 < counter <= 20 -> no text, so that copying or piping is easier

        while (counter > 0)
        {

            Console.WriteLine(Guid.NewGuid());
            counter--;
        }
    }
}
